FROM node:10-alpine

# Define o WORKDIR
WORKDIR /usr/src/app

# Copia os arquivos de dependência
COPY package*.json ./

# Instala as dependências
RUN npm install

# Copia os arquivos gerais
COPY . .

# Compila a aplicação
RUN npm run build

EXPOSE 8000
CMD ["npm", "run", "server"]
