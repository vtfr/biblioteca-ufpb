import React from "react";
import ReactDOM from "react-dom";
import App from "./js/components/App";

import './css/index.css';

// Render
ReactDOM.render(<App />, document.getElementById('root'))
// ReactDOM.render(<Teste />, document.getElementById('root'))
