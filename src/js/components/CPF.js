import React from 'react';

const CPF = ({ cpf }) => {
	const str = String(cpf);
    const res = "0".repeat(11 - str.length) + str;

	const p1 = res.slice(0, 3)
	const p2 = res.slice(3, 6)
	const p3 = res.slice(6, 9)
	const p4 = res.slice(9, 11)

	return (
		<span>{ p1 }.{ p2 }.{ p3 }-{ p4 }</span>
	)
}

export default CPF;
