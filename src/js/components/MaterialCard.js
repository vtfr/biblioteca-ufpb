import React from 'react';
import { Card, Row, Col } from 'antd';
import '../../css/MaterialCard.css';

const MaterialCard = ({ material }) => {
	console.log(material)
	const style = material.estaDisponivel	? { color: "green" } : { color: "red" };

	return (
		<Card type="inner">
			<Row gutter={8}>
				<Col span={4} className="material-th">Situação</Col>
				<Col span={20} style={style}>{material.situacao}</Col>
			</Row>
			<Row gutter={8}>
				<Col span={4} className="material-th">Título</Col>
				<Col span={20}>{material.tituloDto.titulo}</Col>
			</Row>
			<Row gutter={8}>
				<Col span={4} className="material-th">Código de Barras</Col>
				<Col span={20}>{material.codigoBarras}</Col>
			</Row>
			<Row gutter={8}>
				<Col span={4} className="material-th">Status</Col>
				<Col span={20}>{material.status}</Col>
			</Row>
			<Row gutter={8}>
				<Col span={4} className="material-th">Biblioteca</Col>
				<Col span={20}>{material.biblioteca}</Col>
			</Row>
			<Row gutter={8}>
				<Col span={4} className="material-th">Coleção</Col>
				<Col span={20}>{material.colecao}</Col>
			</Row>
			<Row gutter={8} style={{marginTop: "10px"}}>
				<Col span={4} className="material-th">Políticas de Empréstimo</Col>
				<Col span={20}>{material.informacoesPoliticasEmprestimos}</Col>
			</Row>
		</Card>
	);
}

export default MaterialCard;
