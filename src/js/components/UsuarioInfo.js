import React from 'react';

const UsuarioInfo = ({ dadosUsuario, imgUsuarioUrl }) => {
  let fill = null;

  // vinculo = 0 -> Discente
  // vinculo != 0 -> Docente
  // return (<h1>hello</h1>)

  if (dadosUsuario.valorVinculoUsuario === 0) { // Discente
      fill = (
        <table>
          <tbody>
            <tr><td>
                <p className="table-info">DISCENTE</p>
                <p className="table-label">Vínculo</p>
            </td></tr>
            <tr><td>
                <p className="table-info">{dadosUsuario.curso}</p>
                <p className="table-label">Curso</p>
            </td></tr>
            <tr><td>
                <p className="table-info">{dadosUsuario.centro}</p>
                <p className="table-label">Centro</p>
            </td></tr>
            <tr><td>
                <p className="table-info">{dadosUsuario.matricula}</p>
                <p className="table-label">Matrícula</p>
            </td></tr>
          </tbody>
        </table>
      );
  }
  else { // Docente
    let vinculo = null;
    if (dadosUsuario.valorVinculoUsuario === 3) {
      vinculo = "SERVIDOR TÉCNICO ADMINISTRATIVO"
    }
    else if (dadosUsuario.valorVinculoUsuario === 4) {
      vinculo = "DOCENTE"
    }
    fill = (
      <table>
        <tbody>
          <tr><td>
              <p className="table-info">{vinculo}</p>
              <p className="table-label">Vínculo</p>
          </td></tr>
          <tr><td>
            <p className="table-info">{dadosUsuario.siape}</p>
            <p className="table-label">SIAPE</p>
          </td></tr>
          <tr><td>
              <p className="table-info">{dadosUsuario.cargo}</p>
              <p className="table-label">Cargo</p>
          </td></tr>
          <tr><td>
              <p className="table-info">{dadosUsuario.lotacao}</p>
              <p className="table-label">Lotação</p>
          </td></tr>
        </tbody>
      </table>
    );
  }

  return (
    <table>
      <tbody>
        <tr>
          <td style={{ verticalAlign: "top" }}>
            <img className="user-photo" src={imgUsuarioUrl} />
          </td>
          <td style={{ verticalAlign: "top", paddingLeft: '10px'}}>
            <table>
              <tbody>
                <tr><td>
                    <p className="table-info">{dadosUsuario.nome}</p>
                    <p className="table-label">Nome</p>
                </td></tr>
                <tr><td>
                  {fill}
                </td></tr>
                <tr><td>
                    <p className="table-info">{dadosUsuario.idUsuarioBiblioteca}</p>
                    <p className="table-label">ID Biblioteca</p>
                </td></tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
  );
}

export default UsuarioInfo;
