import React, { Component } from 'react';
import { Card, Input, message, Divider, Select } from 'antd';
import UsuarioLista from './UsuarioLista';

import { searchUser, TIPO_CONSULTA_NOME, TIPO_CONSULTA_CPF, TIPO_CONSULTA_SIAPE, TIPO_CONSULTA_MATRICULA } from '../services/sigaa';

class BuscaUsuario extends Component{
	constructor(props) {
		super(props)

		this.state = {
			tipoBusca: TIPO_CONSULTA_NOME,
			textoBusca: "",
			usuarios: null
		}
	}

	onTipoBuscaChange = (tipoBusca) => {
		this.setState({ tipoBusca });
	}

	onSearch = (texto) => {
		searchUser(texto, this.state.tipoBusca, false).then(
			(usuarios) => {
				return this.setState({ usuarios });
			}).catch(err => {
				this.setState({ usuarios: null })
				message.error(err.message);
			});
	}

	render() {
		return (
			<div className='App-dashboard'>
				<Card title="Busca de Usuário" bordered={true}>
					<Input.Group compact>
						<Select
							style={{ width: 120 }}
							defaultValue={ TIPO_CONSULTA_NOME }
							onChange={this.onTipoBuscaChange}>
							<Select.Option value={ TIPO_CONSULTA_CPF }>CPF</Select.Option>
							<Select.Option value={ TIPO_CONSULTA_NOME }>Nome</Select.Option>
							<Select.Option value={ TIPO_CONSULTA_MATRICULA }>Matrícula</Select.Option>
							<Select.Option value={ TIPO_CONSULTA_SIAPE }>SIAPE</Select.Option>
						</Select>
						<Input.Search
							style={{ width: 400 }}
							onSearch={this.onSearch}
						/>
					</Input.Group>

					{(() => {
						console.log(this.state.usuarios);
						if (this.state.usuarios !== null) {
							return (
									<div>
									<Divider orientation="left">
										<span className='soft'>
											Resultado da busca
										</span>
									</Divider>
									<UsuarioLista
										usuarios={this.state.usuarios}
										atualizaUsuario={this.props.onSetUsuario}
									/>
								</div>
							)
						}
					})()}
				</Card>
			</div>
		);
	}
}

export default BuscaUsuario;
