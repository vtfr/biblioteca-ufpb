import React, { Component } from 'react';
import { Card, Input, message, Divider, Button, Icon } from 'antd';

import MaterialCard from './MaterialCard';
import { getMaterialByBarcode } from '../services/sigaa';

class BuscaMaterial extends Component{
	constructor(props) {
		super(props)

		this.state = {
			material: null,
		}
	}

	handleSearch = (query) => {
		if (query.length === 8) {
			getMaterialByBarcode(query).then((material) => {
				return this.setState({ material })
			}).catch(err => {
				message.error(err.message);
				this.setState({ material: null })
			});
		}
		else {
			message.info('Código com tamanho errado');
		}
	}

	render() {
		// console.dir(this.state.material);
		// console.dir(this.props.usuario);
		return (
			<Card title="Busca de Material" bordered={true}>
				<Input.Search
					addonBefore="Código de Barras"
					placeholder="código"
					style={{ width: "320px" }}
					onSearch={ this.handleSearch }
				/>
				{(() => {
					if (this.state.material) {
						let acoes

						if (this.state.material.estaDisponivel) {
							if (this.props.usuario !== null) {
								acoes = (
									<div>
										<Divider orientation="left">
											<span className='soft'>
												Ações
											</span>
										</Divider>
										<h3>Usuário:</h3>
										<p>{this.props.usuario.nome}</p>
										<Button
											onClick={() => { this.props.emprestarMaterial(this.state.material) }}
											type='primary'
											style={{ width: "200px"}}>
											Emprestar<Icon type='right' />
										</Button>
									</div>
								)
							}
						}

						return (
							<div>
								<Divider orientation="left">
									<span className='soft'>
										Resultado da busca
									</span>
								</Divider>
								<MaterialCard material={this.state.material} />
								{acoes}
							</div>
						)
					}
				})()}
			</Card>
		);
	};
}
// {(() => {

// 	)
// })}

export default BuscaMaterial;
