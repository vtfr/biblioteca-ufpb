import React, { Component } from 'react';
import Dashboard from './Dashboard';
import LoginForm from './LoginForm';
import UsuarioPage from './UsuarioCard';
import BuscaUsuario from './BuscaUsuario';
import BuscaMaterial from './BuscaMaterial';
import PaginaPrincipal from './PaginaPrincipal';
import SelecaoBiblioteca from './SelecaoBiblioteca';
import { getUserData, getUserPhoto, returnMaterial, performAction } from '../services/sigaa';
import { message } from 'antd';

import { BrowserRouter, Route, Switch } from 'react-router-dom';

export default class App extends Component {
	constructor() {
		super()

		this.state = {
			loggedIn: false,
			biblioteca: null,
			operador: { },
			usuario: null,
			usuarioFotoUrl: null
		}
	}

	componentDidMount() {
		// Verifica se já está Logado
		const operadorJSON = localStorage.getItem('operador')
		const bibliotecaJSON = localStorage.getItem('biblioteca')
		if (operadorJSON) {
			try {
				const operador = JSON.parse(operadorJSON)
				const biblioteca = JSON.parse(bibliotecaJSON)

				this.setState({ loggedIn: true, operador, biblioteca })
			} catch (e) {
				// Sessão com problema. Remove-a do cliente
				localStorage.removeItem('operador')
				localStorage.removeItem('biblioteca')
			}
		}
	}

	onLogin = (operador) => {
		console.log("Logged in!")
		console.log(operador)

		localStorage.setItem('operador', JSON.stringify(operador))
		this.setState({ loggedIn: true, operador })
	}

	onLogout = () => {
		console.log("Logged out!")

		localStorage.removeItem('operador')
		localStorage.removeItem('biblioteca')
		this.setState({ loggedIn: false, operador: { }, biblioteca: null })
	}

	onSetBiblioteca = (biblioteca) => {
		console.log("Selecionado biblioteca", biblioteca)

		localStorage.setItem('biblioteca', JSON.stringify(biblioteca))
		this.setState({ biblioteca })
	}

	onDevolveMaterial = ({ materialDto, idEmprestimo }) => {
		returnMaterial(materialDto.idMaterial,
			     				 idEmprestimo,
									 this.state.usuario.idUsuarioBiblioteca,
									 this.state.operador.idOperador,
									 this.state.biblioteca.idBiblioteca).then((res) => {
										 console.log(res);
									 }).catch((err) => {
										 console.log(err);
									 })
	}

	onSetUsuario = (idUsuarioBiblioteca) => {
		console.log("Selecionando usuario", idUsuarioBiblioteca)

		getUserData(idUsuarioBiblioteca).then((usuario) => {
			console.log("Usuario encontrado:", usuario);

			getUserPhoto(usuario.idFoto).then((usuarioFotoUrl) => {
				console.log("foto do usuario encontrada:", usuarioFotoUrl)
				this.setState({ usuario, usuarioFotoUrl })
			}).catch((err) => {
				console.log("erro na busca de foto:", err)
			})
		}).catch((err) => {
			console.log("erro na busca de usuario:", err)
		})
	}

	onEmprestimo = (material) => {
		performAction(null, 3, material.idMaterial, 1,
									this.state.usuario.idUsuarioBiblioteca,
									'123456',
									this.state.operador.idOperador,
									this.state.operador.idRegistroEntrada,
									this.state.biblioteca.idBiblioteca)
									.then((res) => {
										console.log(res);
									}).catch((err) => {
										console.log(err);
										message.error(err.message);
									})
	}

	onRenovaMaterial = (material) => {
		console.log(material);
		performAction(material.idEmprestimo, 3, material.materialDto.idMaterial, 2,
									this.state.usuario.idUsuarioBiblioteca,
									'123456',
									this.state.operador.idOperador,
									this.state.operador.idRegistroEntrada,
									this.state.biblioteca.idBiblioteca)
									.then((res) => {
										console.log(res);
									}).catch((err) => {
										console.log(err);
										message.error(err.message);
									});
	}

	onResetUsuario = () => {
		console.log('Resetando usuario');
		this.setState({ usuario: null, usuarioFotoUrl: null })
	}

	render() {
		if (!this.state.loggedIn) {
			return <LoginForm onLogin={this.onLogin} />
		}

		if (this.state.biblioteca === null) {
			if (this.state.operador.bibliotecasPermissaoRemotas.length > 1) {
				console.log('Seleção de libs');
				return <SelecaoBiblioteca
									listaBibliotecas={this.state.operador.bibliotecasPermissaoRemotas}
									onSetBiblioteca={this.onSetBiblioteca}
								/>
			}
			else
				this.setState({ biblioteca: this.state.operador.bibliotecasPermissaoRemotas[0] });
		}

		return (
			<BrowserRouter>
				<Dashboard
					operador={this.state.operador}
					bibAtual={this.state.biblioteca}
					onLogout={this.onLogout}
					onSetBiblioteca={this.onSetBiblioteca}
					onSetUsuario={this.onSetUsuario}>
					 <Switch>
					 	<Route exact path='/' render={() => <PaginaPrincipal operador={this.state.operador} />} />
						<Route exact path='/material' render={() => <BuscaMaterial emprestarMaterial={this.onEmprestimo} usuario={this.state.usuario} />} />
						<Route exact path='/usuario' render={() => {
							if (this.state.usuario === null) {
								return <BuscaUsuario onSetUsuario={this.onSetUsuario} />
							}
							else {
								return <UsuarioPage
												dadosUsuario={this.state.usuario}
												imgUsuarioUrl={this.state.usuarioFotoUrl}
												alteraUsuario={this.onResetUsuario}
												devolveMaterial={this.onDevolveMaterial}
												renovaMaterial={this.onRenovaMaterial}
												/>
							}
						}} />
					 </Switch>
				</Dashboard>
			</BrowserRouter>
		)
	}
}
