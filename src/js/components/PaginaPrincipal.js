import React from 'react';

import { Card, Row, Col } from 'antd';

const PaginaPrincipal = ({ operador }) => (
	<Card title="Bem-vindo" bordered={true}>
		<Row gutter={18}>
			<Col span={12}>
				<p>Bem-vindo, servidor, ao <strong>{ operador.nomeSistema }</strong> ({ operador.siglaSistema }).</p>
				<p>Use os controles acima para selecionar a operação desejada.</p>
			</Col>
			<Col span={12}>
				<pre>
					{ operador.mensagemSobre }
				</pre>
			</Col>
		</Row>
	</Card>
);

export default PaginaPrincipal;
