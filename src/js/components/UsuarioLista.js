import React from 'react';
import { Card, List, Button, Icon, Avatar } from 'antd';
import CPF from './CPF';
import UsuarioAvatar from './UsuarioAvatar';

const UsuarioLista = ({ usuarios, atualizaUsuario }) => {
	return (
		<Card className="scrollable">
			<List
				dataSource={usuarios}
				renderItem={
					(usuario) => (
						<List.Item key={usuario.idUsuarioBiblioteca}>
							<List.Item.Meta
								// avatar={<UsuarioAvatar id={usuario.idUsuarioBiblioteca} size="large" />} // NÃO FUNCIONA!!
								title={usuario.nome}
								description={ <CPF cpf={usuario.cpf} />}
							/>
							<div>
								<Button
									type='primary'
									style={{ width: "130px"}}
									onClick={() => { atualizaUsuario(usuario.idUsuarioBiblioteca) }}>
									Selecionar
									<Icon type="up-circle" />
								</Button>
							</div>
						</List.Item>
					)
				}
			/>
		</Card>
	);
}

export default UsuarioLista;
