import React, { Component } from 'react';
import { Avatar } from 'antd';
import { getUserPhoto } from '../services/sigaa';

class UsuarioAvatar extends Component {
	constructor(props) {
		super(props)
		this.state = {
			// Caso o usuário não tenha avatar, retorna nada
			avatar: "semfoto.png"
		}
	}

	componentDidMount() {
		getUserPhoto(this.props.id).then(avatar => {
			this.setState({ avatar })
		})
	}

	render() {
		return (
			<Avatar src={this.state.avatar} shape="square" size={this.props.size} icon="user" />
		);
	}
}

export default UsuarioAvatar;
