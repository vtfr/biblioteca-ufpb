import React from 'react';
import { Select, Button, Card } from 'antd';
const { Option } = Select;

const SelecaoBiblioteca = ({ listaBibliotecas, onSetBiblioteca }) => {
  let bibliotecaSel = listaBibliotecas[0];
  return (
    <Card className="center-card" title="Seleção de Biblioteca">
      <p>Por favor selecione uma biblioteca para operar:</p>
      <Select
        defaultValue={listaBibliotecas[0].idBiblioteca}
        style={{ width: '100%' }}
        onChange={(id) => {
          bibliotecaSel = listaBibliotecas.find((el) => (el.idBiblioteca === id));
        }}>
          {
            listaBibliotecas.map((biblioteca) => (
              <Option key={biblioteca.idBiblioteca} value={biblioteca.idBiblioteca}>{biblioteca.descricaoBiblioteca}</Option>
            ))
          }
        </Select>
        <div style={{ textAlign: "center", marginTop: 20 }}>
          <Button type="primary" onClick={() => onSetBiblioteca(bibliotecaSel)}>Continuar</Button>
        </div>
    </Card>
  )
}

export default SelecaoBiblioteca;
