import React, { Component } from 'react';
import { Form, Icon, message, Input, Button, Card } from 'antd';

import { login } from '../services/sigaa';

class UnwrappedLoginForm extends Component {

	onSubmit = (e) => {
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (err) {
				message.info('Login Inválido');
				return
			}

			login(values.username, values.password)
				.then((user) => {
					message.info('Logado com sucesso');
					this.props.onLogin(user);
				})
				.catch((error) => {
					message.error('Login ou senha inválidos');
					console.log(error)
				})
		});
	}

	render() {
		const { getFieldDecorator } = this.props.form;

		return (
			<Card className="center-card" title="Login Operador">
				<Form onSubmit={this.onSubmit}>
					<Form.Item>
					{
						getFieldDecorator('username', {
							rules: [{
								required: true,
								message: 'Por favor insira seu usuário!',
							}],
						})(
							<Input prefix={
								<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />
							} placeholder="Usuário" />
						)
					}
					</Form.Item>
					<Form.Item>
						{
							getFieldDecorator('password', {
								rules: [{
									required: true,
									message: 'Por favor insira sua senha!'
								}],
						})(
							<Input prefix={
								<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />
							} type="password" placeholder="Senha" />
						)}
					</Form.Item>
					<Form.Item>
						<Button
							type="primary"
							htmlType="submit"
							className="login-form-button"
							onClick={this.onSubmit}>
							Logar<Icon type="login" />
						</Button>
					</Form.Item>
				</Form>
			</Card>
		);
	}
}

const LoginForm = Form.create()(UnwrappedLoginForm);

export default LoginForm
