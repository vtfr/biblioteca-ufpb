import React from 'react';
import { Link } from 'react-router-dom';
import { Layout, Menu, Button, Dropdown, Col, Row, Icon } from 'antd';
const { Header, Content, Footer } = Layout;

const Dashboard = ({ operador, bibAtual, children, onLogout, onSetBiblioteca }) => {
	return (
		<Layout className="layout">
			<Header>
				<Row>
					<Col span={6}>
						<Dropdown overlay={
								<Menu style={{ maxHeight: "400px", overflowY: "auto" }}>
									{
										operador.bibliotecasPermissaoRemotas.map(biblioteca => (
											<Menu.Item key={ biblioteca.idBiblioteca }>
												<Link to='/' onClick={() => onSetBiblioteca(biblioteca)}>{ biblioteca.descricaoBiblioteca }</Link>
											</Menu.Item>
										))
									}
								</Menu>
							}>
							<a style={{ color: '#ccc' }}>{ bibAtual.descricaoBiblioteca }<Icon type="down" /></a>
						</Dropdown>
					</Col>
					<Col span={12}>
						<h1 id='title'><Link to='/' style={{ color: '#ccc' }}>Biblioteca STI</Link></h1>
					</Col>
					<Col span={6} style={{ textAlign: 'right' }}>
						<Button type="default" ghost onClick={onLogout}>
							<Icon type="logout" /> Sair
						</Button>
					</Col>
				</Row>
			</Header>

			<Menu mode="horizontal">
    		<Menu.Item>
					<Link to="/">Página Principal</Link>
				</Menu.Item>
    		<Menu.Item>
					<Link to="/usuario">Usuário</Link>
				</Menu.Item>
        <Menu.Item>
					<Link to="/material">Material</Link>
				</Menu.Item>
			</Menu>

			<Content className="content">
				<p className="operador-head">
					<strong>Operador:</strong> { operador.nomeOperador }
				</p>
				{children}
			</Content>
			<Footer style={{ textAlign: 'center' }}>
				STI ©2018
			</Footer>
		</Layout>
	);
}

export default Dashboard;
