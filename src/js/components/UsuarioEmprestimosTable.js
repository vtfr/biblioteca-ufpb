import React from 'react';
import { Table, Button, Icon } from 'antd';

const formataData = (inputData) => {
    let data = new Date(inputData);

    let dia = data.getDate();
    if (dia.toString().length === 1)
      dia = "0" + dia;

    let mes = data.getMonth() + 1;
    if (mes.toString().length === 1)
      mes = "0" + mes;

    let ano = data.getFullYear();

    let horas = data.getHours();
    let min = data.getMinutes();
    return dia + '/' + mes + '/' + ano + ' ' + horas + ':' + min;
}

const UsuarioEmprestimosTable = ({ dados, devolveMaterial, renovaMaterial }) => {
  const columns = [{
    title: 'Código de Barras',
    dataIndex: 'materialDto.codigoBarras',
    width: '10%'
  }, {
    title: 'Título',
    dataIndex: 'materialDto.tituloDto.titulo',
    width: '30%'
  }, {
    title: 'Tipo de Emprestimo',
    dataIndex: 'idTipoEmprestimo',
    width: '10%'
  }, {
    title: 'Pode renovar?',
    dataIndex: 'podeRenovar',
    width: '10%'
  }, {
    title: 'Prazo de Devolução',
    dataIndex: 'prazo',
    width: '12%'
  }, {
    title: 'Biblioteca',
    dataIndex: 'materialDto.biblioteca',
    width: '18%'
  },{
    title: 'Devolver',
    render: (index) => (
      <div>
        <Button style={{ width: '100px' }} size="small" onClick={() => {renovaMaterial(index)}}>
          Renovar<Icon type="reload" />
        </Button>
        <Button style={{ width: '100px' }} size="small" onClick={() => {devolveMaterial(index)}}>
          Devolver<Icon type="inbox" />
        </Button>
      </div>
    ),
    width: '10%'
  }];

  let data
  // console.dir(dados);
  if (dados !== undefined) {
    if (dados.acao === undefined){
      data = Object.values({...dados});
      data.forEach(el => {
        el.podeRenovar = el.podeRenovar ? 'sim' : 'não'
        el.prazo = formataData(el.prazo.slice(0, 19) + 'Z')
        el.idTipoEmprestimo = el.idTipoEmprestimo === 3 ? 'NORMAL' : '?????'
      })
    }
    else {
      data = [{...dados}]
      data[0].podeRenovar = data[0].podeRenovar ? 'sim' : 'não'
      data[0].prazo = formataData(data[0].prazo.slice(0, 19) + 'Z')
      data[0].idTipoEmprestimo = data[0].idTipoEmprestimo === 3 ? 'NORMAL' : '?????'
    }
  }
  else {
    data = []
  }

  console.dir(data);
  return (
    <Table
      columns={columns}
      dataSource={data}
      rowKey='idEmprestimo'
      size='small'
      pagination={false}
      scroll={{ y: 500 }} />
  );
}

export default UsuarioEmprestimosTable;
