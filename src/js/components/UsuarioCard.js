import React from 'react';
import { Card, Row, Col, Avatar, Button, Icon, Divider } from 'antd';
import UsuarioInfo from './UsuarioInfo';
import TabelaEmprestimos from './UsuarioEmprestimosTable';

const UsuarioCard = ({ dadosUsuario, imgUsuarioUrl, alteraUsuario, devolveMaterial, renovaMaterial }) => {
	if (dadosUsuario === null) {
		return (
			<Card title="Usuário">
				<h3>Por favor selecione um usuário.</h3>
			</Card>
		)
	}

	return (
		<Card title="Usuário">
			<UsuarioInfo
				dadosUsuario={dadosUsuario}
				imgUsuarioUrl={imgUsuarioUrl}
			/>

      <div style={{textAlign: 'center', paddingTop: '20px'}}>
        <Button href='/usuario' type='primary' style={{ width: "200px"}} onClick={alteraUsuario}>
          Trocar Usuário<Icon type='circle-right' />
        </Button>
      </div>

			<Divider orientation="left">
				<span className='soft'>
					Situação
				</span>
			</Divider>
			{dadosUsuario.descricoesDetalhadasSituacoesUsuario}

			<Divider orientation="left">
				<span className='soft'>
					Empréstimos ativos
				</span>
			</Divider>
			<TabelaEmprestimos dados={dadosUsuario.emprestimos} devolveMaterial={devolveMaterial} renovaMaterial={renovaMaterial} />
		</Card>
	);
}

export default UsuarioCard;
