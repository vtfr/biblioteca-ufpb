import { SIGAA_API_URL, SIGAA_API_BIBLIOTECA_SERVICE } from './config';
import { parseSOAPResponse, createSOAPRequest } from './soap';
import * as md5 from 'md5';

// Retorna um array caso tenha recebido uma hash no formato indice => valor
function fixArray(subject) {
	// Caso seja um objeto...
	if (subject && (subject instanceof Object)) {
		// ...que tenha um valor próprio chamado '0'
		if (subject.hasOwnProperty('0')) {
			// Retorna uma array de objetos
			return Object.values(subject)
		} else {
			// Retorna o objeto em uma array
			return [ subject ]
		}
	}

	// Retorna um array vazio
	return [ ]
}

// Realiza um pedido ao SOAP da biblioteca
function fetchBiblioteca(method, data) {
    // Cria o novo pedido
    const body = createSOAPRequest(method, data)

    // Realiza o pedido via API e começa a cadeia de processamento
    return fetch(SIGAA_API_URL + SIGAA_API_BIBLIOTECA_SERVICE, {
        method: 'POST',
        body: body,
    })
        .then(response => response.text())
        .then(parseSOAPResponse)
        .then((response) => {
            // Verifica se houve algum erro
            if (response.hasOwnProperty('soap:Fault')) {
                throw new Error(response['soap:Fault']['faultstring'])
            }

            // Retorna a responsta
            return response
        })
}

// Realiza o login retornando os dados processados pelo servidor
export function login(username, password) {
    return fetchBiblioteca('logar', {
        arg0: username,
        arg1: md5(password),
        arg2: 'null',
        arg3: 'null',
        arg4: 'null',
    }).then((res) => {
		let user = res['ns1:logarResponse']['return'];

		// Converte o objeto em um array
		user.allTiposEmprestimo = fixArray(user.allTiposEmprestimo)
		user.bibliotecasPermissaoRemotas = fixArray(user.bibliotecasPermissaoRemotas)
		user.papeis = fixArray(user.papeis)

		return user;
	})
}

export const TIPO_CONSULTA_CPF = 1
export const TIPO_CONSULTA_MATRICULA= 2
export const TIPO_CONSULTA_NOME = 3
export const TIPO_CONSULTA_SIAPE = 4

// Pesquisa um usuário
export function searchUser(consulta, tipo, buscarUsuariosExternos) {
    return fetchBiblioteca('buscaUsuariosCirculacaoDesktop', {
        arg0: consulta,
        arg1: tipo,
        arg2: buscarUsuariosExternos,
    }).then(res =>
        fixArray(res['ns1:buscaUsuariosCirculacaoDesktopResponse']['return']))
}

// Retorna os dados do usuario
export function getUserData(id) {
    return fetchBiblioteca('populaDadosUsuario', {
        arg0: {
            idUsuarioBiblioteca: id
        },
    }).then(res => res['ns1:populaDadosUsuarioResponse']['return']);
}

// Retorna a URL com a foto do usuario
export function getUserPhoto(id) {
    return fetchBiblioteca('generateFotoKey', {
        arg0: id,
    }).then(res => {
        const hash = res['ns1:generateFotoKeyResponse']['return'];
        return `${SIGAA_API_URL}verFoto?idFoto=${id}&key=${hash}`;
    });
}

// Procura um material por código de barras
export function getMaterialByBarcode(codigoString, valorVinculoUsuaro) {
    return fetchBiblioteca('findMaterialByCodigoBarras', {
        arg0: codigoString,
        arg1: valorVinculoUsuaro,
    }).then(res => res['ns1:findMaterialByCodigoBarrasResponse']['return']);
}

// Realiza operação de empréstimo/renovação
export const TIPO_ACAO_RENOVAR = 2
export const TIPO_ACAO_EMPRESTAR = 1
export function performAction(idEmprestimo, idTipoEmprestimo, idMaterial,
    tipoAcao, idUsuario, senhaDigitada, idOperador,
    idRegistroEntrada, idBibliotecaOperacao) {

	// Caso seja empréstimo, o idEmprestimo é undefined
	if (tipoAcao === TIPO_ACAO_EMPRESTAR)
		idEmprestimo = undefined;

    return fetchBiblioteca('realizarOperacoes', {
        arg0: {
            idEmprestimo: idEmprestimo,
						idTipoEmprestimo: idTipoEmprestimo,
            materialDto: {
							idMaterial: idMaterial
						},
            acao: tipoAcao,
        },
        arg1: idUsuario,
        arg2: md5(senhaDigitada),
        arg3: idOperador,
        arg4: idRegistroEntrada,
        arg5: idBibliotecaOperacao,
    }).then(res => res['ns1:realizarOperacoesResponse']['return']);
}

// Devolve um material
// TODO verificar se o código de barras é obrigatório ou não
export function returnMaterial(idMaterial, idEmprestimo, idUsuarioBiblioteca,
    idOperador, idBibliotecaOperacao) {

    return fetchBiblioteca('devolverMaterial', {
        arg0: {
            idMaterial,
            emprestimoAtivoMaterial: {
                idEmprestimo,
                usuario: {
                    idUsuarioBiblioteca,
                },
                acao: 0,
            },
        },
        arg1: idOperador,
        arg2: idBibliotecaOperacao
    }).then(res => res['ns1:devolverMaterialResponse']['return']);
}

// Procura empréstimos feitos ao usuário
export function findUserLoans(idUsuario) {
    return fetchBiblioteca('findEmprestimosAtivosUsuario', {
        arg0: idUsuario,
    }).then(res => res['ns1:findEmprestimosAtivosUsuarioResponse']['return']);
}

// Realiza checkout
export function checkout(codigoDeBarras, idOperador) {
    return fetchBiblioteca('realizaCheckout', {
        arg0: codigoDeBarras,
        arg1: idOperador,
    }).then(res => res['ns1:realizaCheckoutResponse']['return']);
}
