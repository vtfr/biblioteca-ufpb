import { js2xml, xml2js } from 'xml-js';

// Remove todos os '{ _text: "texto" }' para o próprio texto contido neles
export function removeUnderscoreText(subject) {
    // Caso subject seja um objeto, itera todas as suas chaves
    if (subject instanceof Object) {
        let obj = { }
        for (let key in subject) {
            // Caso seu seu filho seja um objeto que possua a chave '_text', remove
            // o '_text' e define seu valor para o próprio texto contido nela
            const child = subject[key]
            const hasUnderscoreText = (child instanceof Object) && child.hasOwnProperty("_text")
            obj[key] = hasUnderscoreText ? child._text : removeUnderscoreText(child)
        }

        return obj
    } else {
        // Caso seja outro tipo de valor, apenas retorna o valor
        return subject
    }
}

// Realiza o parsing da string XML no formado SOAP
export function parseSOAPResponse(soapXMLString) {
    // Realiza parsing do XML
    const data = xml2js(soapXMLString, {
        compact: true,
        nativeType: true,
        ignoreCdata: true,
        ignoreAttributes: true,
    })

    // Verifica se a resposta é um objeto JS válido
    if (!(data instanceof Object)) {
        throw new Error("invalid soap XML response:", soapXMLString)
    }

    // Remove os _text e retorna o corpo do SOAP
    return removeUnderscoreText(data['soap:Envelope']['soap:Body'])
}

// Cria um pedido SOAP
export function createSOAPRequest(method, data) {
    // Realiza a serialização da resposta
    return js2xml({
        "soapenv:Envelope": {
            "_attributes": {
                "xmlns:soapenv": "http://schemas.xmlsoap.org/soap/envelope/",
            },
            "soapenv:Body": {
                [method]: data
            }
        }
    }, {
        compact: true,
    })
}
