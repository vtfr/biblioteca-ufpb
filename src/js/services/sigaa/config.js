// Exporta a URL da API do SIGAA. Caso não seja definido, retorna a rota local
// para que seja usado em etapas de desenvolvimento
export const SIGAA_API_URL = process.env.NODE_ENV === 'production'
    ? "http://sigrevisao.sti.ufpb.br/sigaa/"
    : "/sigaa/";

// Exporta o serviço da biblioteca
export const SIGAA_API_BIBLIOTECA_SERVICE = "services/BibliotecaCirculacaoDesktop";
