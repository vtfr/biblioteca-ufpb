const express = require('express');
const path = require('path');
const app = express();

console.log("Preparando servidor")

// Envia os arquivos estáticos
app.use(express.static(path.join(__dirname, '../build')));

// Envia todos os endereços o arquivo index.html
app.get('/*', function (req, res) {
	res.sendFile(path.join(__dirname, '../build', 'index.html'));
});

// Escuta na porta 8000
console.log("Iniciando servidor")
app.listen(8000, '0.0.0.0');
