# Biblioteca STI UFPB

## Configuração do ambiente de desenvolvimento

Primeiro instale as dependências necessárias

    $ npm install

Para executar o servidor de desenvolvimento, rode:

    $ npm run start

## Instalação usando Docker

Primeiro crie a imagem:

    $ docker build -t biblioteca-ufpb .

Depois rode a imagem criada no modo daemon mapeando a porta `8000` usada
internamente pelo servidor para a porta desejada (no caso, foi escolhido
a porta `9000` para fins de exemplo)

	$ docker run -it --rm --name biblioteca-ufpb -p 9000:8000 biblioteca-ufpb

Ou, caso queira executar em modo daemon

	$ docker run -d --rm --name biblioteca-ufpb -p 9000:8000 biblioteca-ufpb

Depois basta acessar `http://localhost:9000` e ver a aplicação rodando.

## Time

* [Johannes Cavalcante](mailto:johanneslpcaval@gmail.com): Programador Frontend
* [Marcello](mailto:prodmarcello@gmail.com): Programador Frontend
* [Otto Victor](mailto:ottovfcb@hotmail.com): Programador Frontend
* [George Nunes](mailto:georgenmoura@gmail.com): Programador Frontend
* [Samuel Pordeus](mailto:samuelspordeus@gmail.com): Programador Frontend
* [Lucas Rincon](mailto:lucas_rincon26@hotmail.com): Programador Frontend
* [Thiago Gomes](mailto:kingtgg@gmail.com): Programador Frontend
* [Natanael Soares](mailto:natanaelrsoares@gmail.com): Documentação da API
* [Elcius Ferreira](mailto:elciusfb2@gmail.com): Documentação da API
* [Victor Franco](mailto:victorfrancovl@gmail.com): DevOps e programador da interface com a API
